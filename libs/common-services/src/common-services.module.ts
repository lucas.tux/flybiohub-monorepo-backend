import { Module } from '@nestjs/common';
import { CommonServicesService } from './common-services.service';

@Module({
  providers: [CommonServicesService],
  exports: [CommonServicesService],
})
export class CommonServicesModule {}
