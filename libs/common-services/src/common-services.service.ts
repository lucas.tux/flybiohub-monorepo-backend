import { Injectable } from '@nestjs/common';

@Injectable()
export class CommonServicesService {
  getCommonInfo(): string {
    return 'CommonServices providing common info...';
  }
}
