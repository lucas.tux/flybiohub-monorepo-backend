import { Test, TestingModule } from '@nestjs/testing';
import { FlybiohubV1Controller } from './flybiohub-v1.controller';
import { FlybiohubV1Service } from './flybiohub-v1.service';

describe('FlybiohubV1Controller', () => {
  let flybiohubV1Controller: FlybiohubV1Controller;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [FlybiohubV1Controller],
      providers: [FlybiohubV1Service],
    }).compile();

    flybiohubV1Controller = app.get<FlybiohubV1Controller>(
      FlybiohubV1Controller,
    );
  });

  describe('root', () => {
    it('should return "Hello World Flybiohub-v1!"', () => {
      expect(flybiohubV1Controller.getHello()).toBe(
        'Hello World Flybiohub-v1!',
      );
    });
  });
});
