import { CommonServicesModule } from '@app/common-services/common-services.module';
import { Module } from '@nestjs/common';
import { FlybiohubV1Controller } from './flybiohub-v1.controller';
import { FlybiohubV1Service } from './flybiohub-v1.service';

@Module({
  imports: [CommonServicesModule],
  controllers: [FlybiohubV1Controller],
  providers: [FlybiohubV1Service],
})
export class FlybiohubV1Module {}
