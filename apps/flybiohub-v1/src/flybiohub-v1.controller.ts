import { CommonServicesService } from '@app/common-services';
import { Controller, Get } from '@nestjs/common';
import { FlybiohubV1Service } from './flybiohub-v1.service';

@Controller()
export class FlybiohubV1Controller {
  constructor(
    private readonly flybiohubV1Service: FlybiohubV1Service,
    private readonly commonService: CommonServicesService,
  ) {}

  @Get()
  getHello(): string {
    return this.flybiohubV1Service.getHello();
  }

  @Get('info')
  getCommonInfo(): string {
    return this.commonService.getCommonInfo();
  }
}
