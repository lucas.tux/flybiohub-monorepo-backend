import { Injectable } from '@nestjs/common';

@Injectable()
export class FlybiohubV1Service {
  getHello(): string {
    return 'Hello World Flybiohub-v1!';
  }
}
