import { NestFactory } from '@nestjs/core';
import { FlybiohubV1Module } from './flybiohub-v1.module';

async function bootstrap() {
  const app = await NestFactory.create(FlybiohubV1Module);
  await app.listen(3001);
}
bootstrap();
