import { CommonServicesModule } from '@app/common-services/common-services.module';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [CommonServicesModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
