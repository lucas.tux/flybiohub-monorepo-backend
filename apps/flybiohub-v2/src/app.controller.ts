import { CommonServicesService } from '@app/common-services/common-services.service';
import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly commonService: CommonServicesService,
  ) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('info')
  getCommonInfo(): string {
    return this.commonService.getCommonInfo();
  }
}
